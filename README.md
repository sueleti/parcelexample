#Ejemplo de Proyecto semilla con Parcel
[Sintáxis](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
##Requisitos previos
Software  
  * IDE intellij
  * node.js v8.10.0
  * npm 6.0.1
  * Grails Version: 3.2.11
  * Groovy Version: 2.4.11
  * JVM Version: 1.8.0_161
  * parcel-bundler@1.8.1

