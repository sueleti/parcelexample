//= wrapped
//= require /angular/angular
//= require /parcelexample/core/parcelexample.core
//= require /parcelexample/index/parcelexample.index

angular.module("parcelexample", [
        "parcelexample.core",
        "parcelexample.index"
    ]);
